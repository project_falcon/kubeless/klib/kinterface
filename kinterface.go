package kinterface

import (
	"fmt"

	"gitlab.com/project_falcon/kubeless/lib/payload"
	"gitlab.com/project_falcon/kubeless/lib/tools"
	k8s "k8s.io/client-go/kubernetes"
	appsV1 "k8s.io/client-go/kubernetes/typed/apps/v1"
	apiV1 "k8s.io/client-go/kubernetes/typed/core/v1"
	v1Beta1 "k8s.io/client-go/kubernetes/typed/extensions/v1beta1"
	restt "k8s.io/client-go/rest"
)

const (
	service = "kinterface"
)

//CreatePODInterface create pod interface
func CreatePODInterface(apiEvent *payload.APIData, namespace string) (*apiV1.PodInterface, error) {
	var podInterface apiV1.PodInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &podInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &podInterface, err
	}

	podInterface = clientset.CoreV1().Pods(namespace)

	return &podInterface, err
}

//CreateSVCInterface create SVC interface
func CreateSVCInterface(apiEvent *payload.APIData, namespace string) (*apiV1.ServiceInterface, error) {
	var svcInterface apiV1.ServiceInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &svcInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &svcInterface, err
	}

	svcInterface = clientset.CoreV1().Services(namespace)

	return &svcInterface, err
}

//CreateINGInterface create ING interface
func CreateINGInterface(apiEvent *payload.APIData, namespace string) (*v1Beta1.IngressInterface, error) {
	var ingInterface v1Beta1.IngressInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &ingInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &ingInterface, err
	}

	ingInterface = clientset.ExtensionsV1beta1().Ingresses(namespace)

	return &ingInterface, err
}

//CreateNodeInterface create node interface
func CreateNodeInterface(apiEvent *payload.APIData) (*apiV1.NodeInterface, error) {
	var nodeInterface apiV1.NodeInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &nodeInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &nodeInterface, err
	}

	nodeInterface = clientset.CoreV1().Nodes()

	return &nodeInterface, nil
}

//CreateNamespaceInterfance create namespace interface
func CreateNamespaceInterfance(apiEvent *payload.APIData) (*apiV1.NamespaceInterface, error) {
	var nameSpaveInterface apiV1.NamespaceInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &nameSpaveInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &nameSpaveInterface, err
	}

	nameSpaveInterface = clientset.CoreV1().Namespaces()

	return &nameSpaveInterface, nil
}

//CreateConfigMapInterfance create configmap interface
func CreateConfigMapInterfance(apiEvent *payload.APIData, namespace string) (*apiV1.ConfigMapInterface, error) {
	var cmInterface apiV1.ConfigMapInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &cmInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &cmInterface, err
	}

	cmInterface = clientset.CoreV1().ConfigMaps(namespace)

	return &cmInterface, nil
}

//CreateSecretInterfance create secret interface
func CreateSecretInterfance(apiEvent *payload.APIData, namespace string) (*apiV1.SecretInterface, error) {
	var secretInterface apiV1.SecretInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &secretInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &secretInterface, err
	}

	secretInterface = clientset.CoreV1().Secrets(namespace)

	return &secretInterface, nil
}

//CreateDeployInterfance create deploy interface
func CreateDeployInterfance(apiEvent *payload.APIData, namespace string) (*appsV1.DeploymentInterface, error) {
	var deployInterface appsV1.DeploymentInterface

	config, err := restt.InClusterConfig()
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot communicate with K8s MASTER Node. Err: %v", err.Error()), 500, tools.ErrColor)
		return &deployInterface, err
	}

	clientset, err := k8s.NewForConfig(config)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot create Clientset. Err: %v", err.Error()), 500, tools.ErrColor)
		return &deployInterface, err
	}

	deployInterface = clientset.AppsV1().Deployments(namespace)

	return &deployInterface, nil
}
