module gitlab.com/project_falcon/kubeless/klib/kinterface

go 1.14

require (
	gitlab.com/project_falcon/kubeless/lib/payload v1.50.3
	gitlab.com/project_falcon/kubeless/lib/tools v0.9.13
	k8s.io/client-go v0.19.4
)
